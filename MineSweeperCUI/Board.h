//
//  Board.h
//  MineSweeperCUI
//
//  Created by dys on 2015/09/02.
//  Copyright (c) 2015年 dys. All rights reserved.
//

#ifndef MineSweeperCUI_Board_h
#define MineSweeperCUI_Board_h

#include "Cell.h"

// ゲーム盤のロジック
class Board {
public:
    Board(int row, int col);
    ~Board();
    bool makeBoard(int difficult);  // 盤面の生成
    bool open(int row, int col);     // cellを開く
    bool flag(int row, int col);     // cellにflagを立てる
    bool check(int row, int col);    // cellに？をつける
    bool isClear();                 // Game Clearしていればtrue
    bool isGameOver();              // Game Overであればtrue
    
    int getMine();                  // 地雷数表示用
    int getRow();                   // 画面表示用
    int getCol();                   // 画面表示用
    Cell* getCell();                // 画面表示用
private:
    int mine;   // 地雷数
    int row;
    int col;
    int difficult;
    Cell *cell;
    
};


#endif
