//
//  Cell.cpp
//  MineSweeperCUI
//
//  Created by dys on 2015/09/03.
//  Copyright (c) 2015年 dys. All rights reserved.
//

#include "Cell.h"

Cell::Cell() {
    state = CLOSE;
    value = 0;
}

bool Cell::open() {
    if(state == OPEN || state == FLAG){
        return false;
    } else {
        state = OPEN;
        return true;
    }
}

bool Cell::toggleFlag() {
    if(state == OPEN){
        return false;
    } else if (state == FLAG){
        state = CLOSE;
        return true;
    } else {
        state = FLAG;
        return true;
    }
}

bool Cell::toggleCheck() {
    if(state == OPEN){
        return false;
    } else if (state == CHECK){
        state = CLOSE;
        return true;
    } else {
        state = CHECK;
        return true;
    }
}

void Cell::setMine() {
    value = MINE;
}

bool Cell::isMine() {
    if(value == MINE){
        return true;
    } else {
        return false;
    }
}

int Cell::getValue() {
    return value;
}

void Cell::setValue(int value){
    this->value = value;
}

int Cell::getState() {
    return state;
}


