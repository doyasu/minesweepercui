//
//  Cell.h
//  MineSweeperCUI
//
//  Created by dys on 2015/09/02.
//  Copyright (c) 2015年 dys. All rights reserved.
//

#ifndef MineSweeperCUI_Cell_h
#define MineSweeperCUI_Cell_h

// cellの値
#define MINE -1

// cellの状態
#define CLOSE 0
#define OPEN 1
#define FLAG 2
#define CHECK 3


// 1マスを表すクラス
//
class Cell {
public:
    Cell();
    bool open();                // cellを開く
    bool toggleFlag();          // flagを切り替える
    bool toggleCheck();         // ?を切り替える
    
    void setMine();             // 地雷をセット
    bool isMine();              // 地雷であればtrue
    
    int getValue();             // cellの値を取得
    void setValue(int value);   // cellに値を代入
    int getState();             // cellの状態を返す
    
private:
    int state;
    int value;
};

#endif
