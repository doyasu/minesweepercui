//
//  main.cpp
//  MineSweeperCUI
//
//  Created by dys on 2015/09/02.
//  Copyright (c) 2015年 dys. All rights reserved.
//

#include <iostream>
#include "Game.h"

using namespace std;

int main() {
    
    Game mineSweeper;
    char command;
    bool isPowerOn = true;      // ゲーム起動中フラグ
    bool isPlayingGame = true;  // 1ゲームをプレイ中のフラグ
    
    
    cout << "マインスイーパー" << endl;
    
    // 新規ゲームを作成
    while(!mineSweeper.init()){
        cout  << endl;
    }
    
    while(isPowerOn){
        
        while (isPlayingGame){
            
            // 画面表示
            mineSweeper.display();
            
            // コマンド入力
            cout << "1. セルを開く（地雷だった場合、失敗）" << endl;
            cout << "2. フラグをON/OFF（フラグがあれば開くことができなくなります）" << endl;
            cout << "3. ？をON/OFF（指定セルに？マークが表示されるだけです）" << endl;
            cout << "9. 諦める" << endl;
            cin >> command;
        
            switch(command){
                
                case '1':
                    // 開く処理
                    mineSweeper.open();
                    break;
                    
                case '2':
                    // フラグの処理
                    mineSweeper.flag();
                    break;
                    
                case '3':
                    // ？の処理
                    mineSweeper.check();
                    break;
                
                case '9':
                    mineSweeper.end();
                    cout << "Game Over" << endl;
                    isPlayingGame = false;
                    break;
            
                default:
                    cout << "不正なコマンドです" << endl;
                    break;
            }
            
            // コマンド9で終了時に下部の処理をさけるため
            if(!isPlayingGame){
                continue;
            }
        
            if (mineSweeper.isClear()){
                mineSweeper.end();
                cout << "Game Clear" << endl;
                isPlayingGame = false;
            } else if (mineSweeper.isGameOver()){
                mineSweeper.end();
                cout << "Game Over" << endl;
                isPlayingGame = false;
            }
            
        } // isPlayingGame
        
        
        cout << "新しいゲームを始めますか？ y / n" << endl;
        cin >> command;

        if( command == 'y' ){
            // 新規ゲームを作成
            while(!mineSweeper.init()){
                cout  << endl;
            }
            isPlayingGame = true;
        } else if (command == 'n'){
            cout << "ゲームを終了します" << endl;
            isPowerOn = false;
        } else {
            cout << "不正なコマンドです" << endl;
        }
        
    } // isPowerOn
    
    
    return 0;
}
