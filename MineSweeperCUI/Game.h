//
//  Game.h
//  MineSweeperCUI
//
//  Created by dys on 2015/09/03.
//  Copyright (c) 2015年 dys. All rights reserved.
//

#ifndef MineSweeperCUI_Game_h
#define MineSweeperCUI_Game_h

#include "Board.h"

// UIを提供するクラス
class Game {
public:
    Game();
    bool init();    // ゲーム盤作成のための初期化処理
    void end();     // ゲーム盤を再生成するための終了処理
    void display(); // 画面表示
    void open();    // cellを開く
    void flag();    // cellにflagを立てる
    void check();   // cellに？をつける
    bool isClear(); // ゲームクリアしたかどうか
    bool isGameOver();  // ゲームオーバーしたかどうか
    
private:
    Board *board;
};

#endif
