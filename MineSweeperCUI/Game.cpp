//
//  Game.cpp
//  MineSweeperCUI
//
//  Created by dys on 2015/09/03.
//  Copyright (c) 2015年 dys. All rights reserved.
//

#include "Game.h"

#include <iostream>
#include <iomanip>

using namespace std;

Game::Game(){
    board = nullptr;
}

bool Game::init(){
    int row, col, difficult;
    
    cout << "ゲーム盤のサイズ(2〜9)を入力してください > (row, col) = ";
    cin >> row >> col;
    
    if(row < 0 || col < 0){
        cout << "負の数は入力できません" << endl;
        return false;
    }
    if(row == 0 || col == 0){
        cout << "行または列が0のゲームは生成できません" << endl;
        return false;
    }
    if(row == 1 || col == 1){
        cout << "行または列が1のゲームは生成できません" << endl;
        return false;
    }
    if(row >= 10 || col >= 10){
        cout << "ゲーム盤が大きすぎます。UIが悪いのでたぶんクリアできません。" << endl;
        return false;
    }
    
    cout << "ゲームの難易度(2 〜 n)を入力してください(2が最高難度)" << endl;
    cout << "(ゲーム盤の大きさによりますが、最大10程度にしておいてください)" << endl;
    cout << "> ";
    cin >> difficult;
    
    if (difficult < 2){
        cout << "2以下の難易度は設定できません" << endl;
        return false;
    }
    
    // ゲーム盤生成
    board = new Board(row, col);
    
    // ゲームの初期化
    while( !board->makeBoard(difficult) ){
        cout << "ゲーム盤生成に失敗したため、再生成を行います" << endl;
    }
    
    return true;
    
}

void Game::end(){
    Cell *cell = board->getCell();
    int row = board->getRow();
    int col = board->getCol();
    
    cout << endl;
    cout << " y|" << endl;
    for(int i=1; i<=row; i++){
        cout << right << setw(2) << i-1 << "|";
        for(int j=1; j<=col; j++){
            if((cell + i * (col+2) + j)->isMine()){
                cout << "■";
            } else {
                cout << (cell + i * (col+2) + j)->getValue();
            }
        }
        cout << endl;
    }
    cout << "  -";
    for(int j=0;j<col;j++){
        cout << "-";
    }
    cout << endl;
    cout << " x ";
    for (int j=0;j<col;j++){
        cout << j;
    }
    cout << endl;
    
    cout << endl;
    
    cout << "地雷残存数 : " << board->getMine() << endl;
    
    cout << endl;
    
    delete board;
    board = nullptr;
}

void Game::display(){
    
    Cell *cell = board->getCell();
    int row = board->getRow();
    int col = board->getCol();
    
    cout << endl;
    cout << " y|" << endl;
    for(int i=1; i<=row; i++){
        cout << right << setw(2) << i-1 << "|";
        for(int j=1; j<=col; j++){
            if( (cell + i * (col+2) + j)->getState() == OPEN ){
                if((cell + i * (col+2) + j)->isMine()){
                    cout << "■";
                } else {
                    cout << (cell + i * (col+2) + j)->getValue();
                }
            } else if ((cell + i * (col+2) + j)->getState() == FLAG) {
                cout << "F";
            } else if ((cell + i * (col+2) + j)->getState() == CHECK) {
                cout << "?";
            } else {
                cout << "□";
            }
        }
        cout << endl;
    }
    cout << "  -";
    for(int j=0;j<col;j++){
        cout << "-";
    }
    cout << endl;
    cout << " x ";
    for (int j=0;j<col;j++){
        cout << j;
    }
    cout << endl;
    
    cout << endl;
    
    cout << "地雷残存数 : " << board->getMine() << endl;
    
    cout << endl;
    
}

void Game::open(){
    int row, col;
    
    cout << "開くセルの位置を指定してください > (x, y) = ";
    cin >> col >> row;
    
    if( row > board->getRow() || row < 0 || col > board->getCol() || col < 0){
        cout << "範囲外のセルです" << endl;
        return;
    }
    
    if( !board->open(row, col) ){
        cout << "すでに開かれているか、フラグがたっています" << endl;
    }
    
}

void Game::flag(){
    int row, col;
    
    cout << "FlagをON/OFFするセルの位置を指定してください > (x, y) = ";
    cin >> col >> row;
    
    if( row > board->getRow() || row < 0 || col > board->getCol() || col < 0){
        cout << "範囲外のセルです" << endl;
        return;
    }
    
    if( !board->flag(row, col) ){
        cout << "すでに開かれています" << endl;
    }

}

void Game::check(){
    int row, col;
    
    cout << "?をON/OFFするセルの位置を指定してください > (x, y) = ";
    cin >> col >> row;
    
    if( row > board->getRow() || row < 0 || col > board->getCol() || col < 0){
        cout << "範囲外のセルです" << endl;
        return;
    }
    
    if( !board->check(row, col) ){
        cout << "すでに開かれています" << endl;
    }
    
}

bool Game::isClear(){
    return board->isClear();
}

bool Game::isGameOver(){
    return board->isGameOver();
}