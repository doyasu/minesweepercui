//
//  Board.cpp
//  MineSweeperCUI
//
//  Created by dys on 2015/09/03.
//  Copyright (c) 2015年 dys. All rights reserved.
//

#include "Board.h"
#include <iostream>
#include <random>
#include <iomanip>

using namespace std;

Board::Board(int row, int col)
    : row(row), col(col), difficult(0) {

        // カウントを楽するために、本来より一回り多く生成
        cell = new Cell[(row+2)*(col+2)];
        
        mine = 0;
}

Board::~Board(){
    delete []cell;
}

bool Board::makeBoard(int difficult){
    
    random_device rd;
    mt19937 mt(rd());
    
    // ランダム数 % 難易度 ==2 2 0のセルは地雷を埋める
    mine = 0;
    for(int i=1; i<=row; i++){
        for(int j=1; j<=col; j++){
            if ( mt() % difficult == 0 ){
                (cell + i * (col+2) + j)->setMine();
                mine++;
            }
        }
    }
    
    if(mine == row*col || mine == 0){
        return false;
    }
    
    // 周囲の地雷の数をカウント
    int count;
    for(int i=1; i<=row; i++){
        for(int j=1; j<=col; j++){
            if((cell + i * (col+2) + j)->isMine()){
                continue;
            }
            count = 0;
            if((cell + (i-1) * (col+2) + j-1)->isMine()) count++;
            if((cell + (i-1) * (col+2) + j  )->isMine()) count++;
            if((cell + (i-1) * (col+2) + j+1)->isMine()) count++;
            if((cell +  i    * (col+2) + j-1)->isMine()) count++;
            if((cell +  i    * (col+2) + j+1)->isMine()) count++;
            if((cell + (i+1) * (col+2) + j-1)->isMine()) count++;
            if((cell + (i+1) * (col+2) + j  )->isMine()) count++;
            if((cell + (i+1) * (col+2) + j+1)->isMine()) count++;
            
            (cell + i * (col+2) + j)->setValue(count);
        }
    }
    
    return true;
}

bool Board::open(int row, int col){
    row++;
    col++;
    
    return (cell + row * (this->col+2) + col)->open();
    
}

bool Board::flag(int row, int col){
    row++;
    col++;
    
    return (cell + row * (this->col+2) + col)->toggleFlag();
}

bool Board::check(int row, int col){
    row++;
    col++;
    
    return (cell + row * (this->col+2) + col)->toggleCheck();
}

bool Board::isClear() {
    int count = 0;
    for(int i=1; i<=row; i++){
        for(int j=1; j<=col; j++){
            // OpenなCellの数をカウント
            if( (cell + i * (col+2) + j)->getState() == OPEN){
                count++;
            }
        }
    }
    
    // （全セル数 == 地雷数 + OpenなCellの数）であればGame Clear
    if( (row * col) == (mine + count)){
        return true;
    } else {
        return false;
    }
}

bool Board::isGameOver() {
    for(int i=1; i<=row; i++){
        for(int j=1; j<=col; j++){
            
            // CellがOpenかつ地雷であればGame Over
            if( (cell + i * (col+2) + j)->getState() == OPEN &&
                (cell + i * (col+2) + j)->isMine() ){
                    return true;
            }
        }
    }
    return false;
}

int Board::getMine() {
    return mine;
}

int Board::getRow() {
    return row;
}

int Board::getCol() {
    return col;
}

Cell* Board::getCell() {
    return cell;
}

